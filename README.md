# timespec-arith

C library for `timespec` arithmetic.

## Features

- [ ] addition
- [ ] subtraction
    - [ ] absolute difference
- [ ] multiplication
- [ ] division
- [ ] conversions: nanos <-> millis, micros, double/float combined seconds, etc.

## Time point vs Duration

`timespec` can represent a point in time (e.g. unix epoch) or an interval (duration). Arithmetic operations make sense only for intervals/durations.

## References

- [gcc: Calculating Elapsed Time](https://www.gnu.org/software/libc/manual/html_node/Calculating-Elapsed-Time.html) shows how to subtract two `timeval`s. Can be adapted to `timespec` as well.

## License

MIT. See LICENSE
