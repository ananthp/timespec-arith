#include <stdio.h>
#include <time.h>

int main(void)
{
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    printf("Timestamp sec: %ld nanos: %ld\n", ts.tv_sec, ts.tv_nsec);
}
